package com.example.calculator205150401111052;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class HasilActivity extends AppCompatActivity {
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_output);

        hasil = findViewById(R.id.bsama);
        Intent intent = getIntent();
        int hasil1 = intent.getIntExtra("hasil", 0);
        String hasil2 = String.valueOf(hasil1);
        hasil.setText(intent.getStringExtra("operasi") + " " + hasil2);


    }
}
