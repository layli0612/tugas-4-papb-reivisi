package com.example.calculator205150401111052;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editTextLayer;
    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b0;
    Button btambah, bkurang, bkali, bbagi, bsama;
    Button benter;
    TextView opAwal;

    public static double nilaiSekarang=0.0;
    public static double hasil=0.0;
    public static String operasiSekarang="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        init();
        opAwal = findViewById(R.id.opAwal);
    }

    void init(){
        editTextLayer = (EditText) findViewById(R.id.editTextLayar);
        opAwal = findViewById(R.id.opAwal);
        b0 = (Button) findViewById(R.id.b0);
        b0.setOnClickListener(this);
        b1 = (Button) findViewById(R.id.b1);
        b1.setOnClickListener(this);
        b2 = (Button) findViewById(R.id.b2);
        b2.setOnClickListener(this);
        b3 = (Button) findViewById(R.id.b3);
        b3.setOnClickListener(this);
        b4 = (Button) findViewById(R.id.b4);
        b4.setOnClickListener(this);
        b5 = (Button) findViewById(R.id.b5);
        b5.setOnClickListener(this);
        b6 = (Button) findViewById(R.id.b6);
        b6.setOnClickListener(this);
        b7 = (Button) findViewById(R.id.b7);
        b7.setOnClickListener(this);
        b8 = (Button) findViewById(R.id.b8);
        b8.setOnClickListener(this);
        b9 = (Button) findViewById(R.id.b9);
        b9.setOnClickListener(this);

        btambah = (Button) findViewById(R.id.btambah);
        btambah.setOnClickListener(this);
        bkurang = (Button) findViewById(R.id.bkurang);
        bkurang.setOnClickListener(this);
        bkali = (Button) findViewById(R.id.bkali);
        bkali.setOnClickListener(this);
        bbagi = (Button) findViewById(R.id.bbagi);
        bbagi.setOnClickListener(this);

        bsama = (Button) findViewById(R.id.bsama);
        bsama.setOnClickListener(this);
        benter = (Button) findViewById(R.id.benter);
        benter.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Button button = (Button) v;
        String buttonText = button.getText().toString();
        String operasiData = opAwal.getText().toString();

        operasiData = operasiData+buttonText;

        opAwal.setText(operasiData);
        switch (v.getId()){
            case R.id.b0:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"0");
                break;
            case R.id.b1:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"1");
                break;
            case R.id.b2:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"2");
                break;
            case R.id.b3:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"3");
                break;
            case R.id.b4:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"4");
                break;
            case R.id.b5:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"5");
                break;
            case R.id.b6:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"6");
                break;
            case R.id.b7:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"7");
                break;
            case R.id.b8:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"8");
                break;
            case R.id.b9:
                editTextLayer.setText(editTextLayer.getText().toString().trim()+"9");
                break;


            case R.id.btambah:
                if (editTextLayer.getText().toString().trim().equals("")){
                    Toast.makeText(MainActivity.this, "Angka tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                operasiSekarang="tambah";
                nilaiSekarang = Double.parseDouble(editTextLayer.getText().toString());
                editTextLayer.setText("");
                break;
            case R.id.bkurang:
                if (editTextLayer.getText().toString().trim().equals("")){
                    Toast.makeText(MainActivity.this, "Angka tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                operasiSekarang="kurang";
                nilaiSekarang = Double.parseDouble(editTextLayer.getText().toString());
                editTextLayer.setText("");
                break;
            case R.id.bkali:
                if (editTextLayer.getText().toString().trim().equals("")){
                    Toast.makeText(MainActivity.this, "Angka tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                operasiSekarang="kali";
                nilaiSekarang = Double.parseDouble(editTextLayer.getText().toString());
                editTextLayer.setText("");
                break;
            case R.id.bbagi:
                if (editTextLayer.getText().toString().trim().equals("")){
                    Toast.makeText(MainActivity.this, "Angka tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                operasiSekarang="bagi";
                nilaiSekarang = Double.parseDouble(editTextLayer.getText().toString());
                editTextLayer.setText("");
                break;

            case R.id.benter:
                nilaiSekarang=0;
                editTextLayer.setText("");
                break;

            case R.id.bsama:
                if(operasiSekarang.equals("tambah")){
                    hasil = nilaiSekarang + Double.parseDouble(editTextLayer.getText().toString().trim());
                    editTextLayer.setText(String.valueOf(hasil));
                }
                if(operasiSekarang.equals("kurang")){
                    hasil = nilaiSekarang - Double.parseDouble(editTextLayer.getText().toString().trim());
                    editTextLayer.setText(String.valueOf(hasil));
                }
                if(operasiSekarang.equals("kali")){
                    hasil = nilaiSekarang * Double.parseDouble(editTextLayer.getText().toString().trim());
                    editTextLayer.setText(String.valueOf(hasil));
                }
                if(operasiSekarang.equals("bagi")){
                    hasil = nilaiSekarang / Double.parseDouble(editTextLayer.getText().toString().trim());
                    editTextLayer.setText(String.valueOf(hasil));
                }
                break;
        }
    }
}